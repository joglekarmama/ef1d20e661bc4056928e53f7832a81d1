import React, { useState, Fragment, useContext } from 'react';
import { GameContext } from '../../context/gameContext';
import { ScoreContext } from '../../context/scoreContext';
import Styled from 'styled-components';
import config from 'visual-config-exposer';

import './quizCards.css';

const questions = config.quizSettings.quizQuestions;
const questionArray = [];

for (let keys of Object.keys(questions)) {
  questionArray.push(questions[keys]);
}

const QuestionContainer = Styled.div`
 background-color: ${config.quizSettings.cardColor};
`;

const Question = Styled.div`
 color: ${config.quizSettings.questionColor};
`;

const Option = Styled.div`
  background-color: ${config.quizSettings.optionBgColor};
  border-radius: 15px;
  color: ${config.quizSettings.optionColor};

  :hover {
    transform: scale(1.03);
  }
`;

const QuizCards = (props) => {
  const [questionNumber, setQuestionNumber] = useState(0);
  const gameContext = useContext(GameContext);
  const scoreContext = useContext(ScoreContext);

  const checkScore = () => {
    const selected = document.querySelectorAll('.selected');
    selected.forEach((el) => {
      if (el.dataset.correct === 'true') {
        scoreContext.setScore(config.quizSettings.correctPoints);
        if (config.quizSettings.showAnswer) {
          el.classList.remove('selected');
          el.classList.add('correct');
        }
      } else {
        scoreContext.setScore(config.quizSettings.wrongPoints * -1);
        if (config.quizSettings.showAnswer) {
          el.classList.remove('selected');
          el.classList.add('wrong');
        }
      }
    });
    if (config.quizSettings.showAnswer) {
      setTimeout(changeQuestionHandler, 1000);
    } else {
      setTimeout(changeQuestionHandler, 100);
    }
  };

  const changeQuestionHandler = () => {
    const selected = document.querySelectorAll('.selected');
    const correct = document.querySelectorAll('.correct');
    const wrong = document.querySelectorAll('.wrong');
    selected.forEach((el) => {
      el.classList.remove('selected');
      el.classList.remove('correct');
      el.classList.remove('wrong');
    });
    correct.forEach((el) => {
      el.classList.remove('selected');
      el.classList.remove('correct');
      el.classList.remove('wrong');
    });
    wrong.forEach((el) => {
      el.classList.remove('selected');
      el.classList.remove('correct');
      el.classList.remove('wrong');
    });
    console.log(questionNumber, questionArray.length - 1);
    if (questionNumber === questionArray.length - 1) {
      props.quizEnd('COMPLETED');
    }
    setQuestionNumber(questionNumber + 1);
  };

  const SelectHandler = (event) => {
    if (!event.target.classList.contains('selected')) {
      event.target.classList.add('selected');
    } else {
      event.target.classList.remove('selected');
    }
  };

  const questionJsx = (
    <Fragment>
      <div className="question_container">
        <Question className="question-no">
          Questions {questionNumber + 1}/{questionArray.length}
        </Question>
        <Question className="question">
          {questionArray[questionNumber].question}
        </Question>
      </div>
      <div className="option_container">
        <Option
          className="option"
          onClick={SelectHandler}
          data-correct={questionArray[questionNumber].option1.correct}
        >
          {questionArray[questionNumber].option1.optionText}
        </Option>
        <Option
          className="option"
          onClick={SelectHandler}
          data-correct={questionArray[questionNumber].option2.correct}
        >
          {questionArray[questionNumber].option2.optionText}
        </Option>
        <Option
          className="option"
          onClick={SelectHandler}
          data-correct={questionArray[questionNumber].option3.correct}
        >
          {questionArray[questionNumber].option3.optionText}
        </Option>
        <Option
          className="option"
          onClick={SelectHandler}
          data-correct={questionArray[questionNumber].option4.correct}
        >
          {questionArray[questionNumber].option4.optionText}
        </Option>
      </div>
    </Fragment>
  );

  return (
    <Fragment>
      <QuestionContainer className="question__container">
        {questionJsx}
        <button
          style={{
            color: config.quizSettings.nextBtnTextColor,
            backgroundColor: config.quizSettings.nextBtnColor,
          }}
          className="next__button"
          onClick={checkScore}
        >
          Next
        </button>
      </QuestionContainer>
      <style>{`
          .selected {
            transform: scale(1.03);
            border: 1px solid ${config.quizSettings.highlightColor};
          }

          .correct {
            border: 1px solid ${config.quizSettings.correctColor};
          }

          .wrong {
            border: 1px solid ${config.quizSettings.wrongColor};
          }
          `}</style>
    </Fragment>
  );
};

export default QuizCards;
